import {isMobile} from "../../functions/mobile"

$(function () {

    if ( $('body').width() < 768 ) {
        console.log('mobile');
        
        $('._tipbar').slick({
            autoplay: true,
            autoplaySpeed: 4000,
            infinite: false,
            arrows: false,
            dots: false,
            slidesToShow: 1,
            slidesToScroll: 1
        });
    }

    $(".slideshow").each(function() {

        var $arrows = ($(this).data("arrow") != null ? $(this).data("arrow") : true),
            $dots = ($(this).data("dots") != null ? $(this).data("dots") : false),
            $qtd = ($(this).data("qtd") != null || $(this).data("qtd") != undefined) ? $(this).data("qtd") : 4,
            $auto = ($(this).data("auto") != null ? true : false);

        var settings = {
            prevArrow: '<a class="slick-prev"></a>',
            nextArrow: '<a class="slick-next"></a>',
            arrows: $arrows,
            slidesToShow: ($(this).children().length >= $qtd ? $qtd : $(this).children().length),
            accessibility: false,
            autoplay: $auto,
            autoplaySpeed: 6000,
            infinite:true,
            responsive: [
                {
                    breakpoint: 935,
                    settings: {
                        slidesToShow: ($qtd == 1) ? 1 : 2
                    }
                },
                {
                    breakpoint: 724,
                    settings: {
                        slidesToShow: ($qtd == 1) ? 1 : 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        };
        var $slider = jQuery(this);

        if(!isMobile()) {
            ($(this).children().length >= $qtd ? $slider.slick(settings) : "");
        } else {
            $slider.slick(settings)
        }
    });

});

