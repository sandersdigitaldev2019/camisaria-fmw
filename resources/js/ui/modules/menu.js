import { isMobile } from '../../functions/mobile';

/**
 * Módulo Mega Menu
 * Montagem do Mega Menu
 */
document.addEventListener("touchstart", function () {
}, false);

$(function () {


    $('.wsnavtoggle').click(function () {

        $('.pusher').toggleClass('wsoffcanvasopener');

        if($('.pusher').hasClass("wsoffcanvasopener")) {
            $(".menuheader").animate({
                left: 0
            }).removeClass("active")
        } else {
            $(".menuheader").animate({
                left: -($(".menuheader").outerWidth())
            }).addClass("active")
        }

        $('.wsmenu > .menu > li > a').on('click', function(e) {
            if($(this).siblings().hasClass("megamenu")) {
                e.preventDefault()


                $(".titleMenu .title").text($(this).text())

                $(".wsmenu-submenu, .megamenu").not($(this).siblings('.wsmenu-submenu, .megamenu')).slideUp('slow');
                //$(this).siblings('.wsmenu-submenu').slideToggle('slow');
                $(this).siblings('.megamenu').show();

                $(".megamenu .nivel").on("click", function(e) {
                    if($(this).siblings().hasClass("subMega")) {
                        e.preventDefault()

                        var _t = $(this);



                        if ($(this).hasClass("active")) {
                            $(this).siblings(".subMega").slideUp('slow', function () {
                                _t.removeClass("active")
                            });

                        } else {
                            $(this).siblings(".subMega").slideDown('slow', function () {
                                _t.addClass("active")
    });
                        }

                    }
                })

                $(this).parents("ul").animate({
                    left: -($(this).siblings(".megamenu").outerWidth())
                }).addClass("active")
            }
        })

        $('.backLink').on('click', function(e) {

            $(".menuheader .megamenu").hide();
            $(".subMega").hide()
            $(".megamenu .column, .megamenu .nivel").removeClass("active")

            $(".titleMenu .title").text("Escolha uma categoria")

            $(this).parents("ul").animate({
                left: 0
            }).addClass("active")

        });

    });

    $('.overlapblackbg').click(function () {
        $('.pusher').toggleClass('wsoffcanvasopener');
    });

    $('.wsmenu-list > li').has('.megamenu').children("a").prepend('<span class="wsmenu-click"><i class="wsmenu-arrow icon angle down"></i></span>');
    $('.wsmenu-list .megamenu').has('.subMega').find(".nivel").prepend('<span class="wsmenu-click"><i class="wsmenu-arrow icon angle down"></i></span>');

    if ($(window).innerWidth() <= 1024) {
        $('.wsmenu-list > li > a').click(function (e) {
            ($(this).siblings(".megamenu").length > 0 ? e.preventDefault() : "")
        $(this).toggleClass('ws-activearrow').parent().siblings().children().removeClass('ws-activearrow');
        $(".wsmenu-submenu, .megamenu").not($(this).siblings('.wsmenu-submenu, .megamenu')).slideUp('slow');
            $(this).siblings('.wsmenu-submenu').fadeToggle('slow');
            $(this).siblings('.megamenu').fadeToggle('slow');
        });
    }

    $('.wsmenu').swipeleft(function () {
        $('.pusher').toggleClass('wsoffcanvasopener');
    });

    /*

    $('.wsmenu-list > li').on("mouseover", function () {
        $(".maskBG").height($(".megamenu", this).outerHeight() + 20);
    });
    
    */

});